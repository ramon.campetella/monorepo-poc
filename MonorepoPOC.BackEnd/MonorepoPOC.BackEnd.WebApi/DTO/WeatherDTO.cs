﻿namespace MonorepoPOC.BackEnd.WebApi.DTO
{
    public class WeatherDTO
    {
        public double Temperature { get; set; }
        public double FeelsLike { get; set; }
        public double Pressure { get; set; }
        public double Humidity { get; set; }
    }
}

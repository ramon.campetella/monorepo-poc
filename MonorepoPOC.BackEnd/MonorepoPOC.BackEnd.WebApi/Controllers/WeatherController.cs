﻿using Microsoft.AspNetCore.Mvc;
using Monorepo.BackEnd.Domain;
using MonorepoPOC.BackEnd.Interfaces.Application;
using MonorepoPOC.BackEnd.WebApi.DTO;
using System.Net.Http;

namespace MonorepoPOC.BackEnd.WebApi.Controllers
{
    public class WeatherController : Controller
    {
        private protected IWeatherService _weatherService;
        public WeatherController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("weather")]
        public async Task<WeatherDTO> GetWeatherAsync(double latitude, double longitude)
        {
            
            var weather = await _weatherService.GetWeatherAsync(latitude, longitude);
            WeatherDTO weatherDTO = this.MapToWeather(weather);
            
            return weatherDTO;
        }

        private WeatherDTO MapToWeather(Weather weather)
        {
            return new WeatherDTO
            {
                Temperature = weather.Temperature,
                FeelsLike = weather.FeelsLike,
                Pressure = weather.Pressure,
                Humidity = weather.Humidity
            };
        }
    }
}

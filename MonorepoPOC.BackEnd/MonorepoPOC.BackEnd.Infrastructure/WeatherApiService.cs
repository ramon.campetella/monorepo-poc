﻿using Monorepo.BackEnd.Domain;
using MonorepoPOC.BackEnd.Infrastructure.ExternalsDTO;
using MonorepoPOC.BackEnd.Interfaces.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace MonorepoPOC.BackEnd.Infrastructure
{
    public class WeatherApiService : IWeatherApiService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        //private readonly HttpClient _httpClient;

        public WeatherApiService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<Weather> GetWeatherAsync(double latitude, double longitude)
        {
            
            var apiKey = "a1360f4e7200d649873648dabacfd2df";
            var apiUrl = $"https://api.openweathermap.org/data/3.0/onecall?lat={latitude}&lon={longitude}&appid={apiKey}&units=metric&exclude=daily,minutely,hourly,alerts";
            
            var httpClient = _httpClientFactory.CreateClient();
            var response = await httpClient.GetAsync(apiUrl);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Error retrieving weather data");
            }

            var content = await response.Content.ReadAsStringAsync();
            var weatherData = JsonConvert.DeserializeObject<WeatherData>(content);

            if (weatherData == null)
            {
                throw new Exception("No data found");
            }

            return MapToWeather(weatherData);
            
        }

        private Weather MapToWeather(WeatherData data)
        {
            return new Weather
            {
                Temperature = data.Current.Temp,
                FeelsLike = data.Current.FeelsLike,
                Pressure = data.Current.Pressure,
                Humidity = data.Current.Humidity
            };
        }
    }
}
﻿using Monorepo.BackEnd.Domain;
using MonorepoPOC.BackEnd.Interfaces.Application;
using MonorepoPOC.BackEnd.Interfaces.Infrastructure;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace Monorepo.BackEnd.Application
{
    public class WeatherService : IWeatherService
    {
        private protected readonly IWeatherApiService _weatherApiService;
       
        public WeatherService(IWeatherApiService weatherApiService)
        {
            _weatherApiService = weatherApiService;
        }

        public async Task<Weather> GetWeatherAsync(double latitude, double longitude)
        {
            return await _weatherApiService.GetWeatherAsync(latitude, longitude);
        }
    }
}
﻿using Monorepo.BackEnd.Application;
using Monorepo.BackEnd.Domain;
using MonorepoPOC.BackEnd.Interfaces.Infrastructure;
using Moq;


namespace MonorepoPOC.BackEnd.Tests
{
    public class WeatherServiceTests
    {
        [Fact]
        public async Task GetWeatherAsync_ReturnsWeatherData_WhenSuccessful()
        {
            // Arrange
            var latitude = -34.6132;
            var longitude = -58.3772;
            var expectedWeather = new Weather
            {
                Temperature = 21.22,
                FeelsLike = 21.43,
                Pressure = 1004,
                Humidity = 78
            };

            var weatherApiServiceMock = new Mock<IWeatherApiService>();
            weatherApiServiceMock
                .Setup(service => service.GetWeatherAsync(latitude, longitude))
                .ReturnsAsync(expectedWeather);

            var weatherService = new WeatherService(weatherApiServiceMock.Object);

            // Act
            var weather = await weatherService.GetWeatherAsync(latitude, longitude);

            // Assert
            Assert.NotNull(weather);
            Assert.Equal(expectedWeather.Temperature, weather.Temperature);
            Assert.Equal(expectedWeather.FeelsLike, weather.FeelsLike);
            Assert.Equal(expectedWeather.Pressure, weather.Pressure);
            Assert.Equal(expectedWeather.Humidity, weather.Humidity);
        }

        [Fact]
        public async Task GetWeatherAsync_ThrowsException_WhenWeatherApiServiceFails()
        {
            // Arrange
            var latitude = -34.6132;
            var longitude = -58.3772;

            var weatherApiServiceMock = new Mock<IWeatherApiService>();
            weatherApiServiceMock
                .Setup(service => service.GetWeatherAsync(latitude, longitude))
                .ThrowsAsync(new Exception("Weather API service error"));

            var weatherService = new WeatherService(weatherApiServiceMock.Object);

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => weatherService.GetWeatherAsync(latitude, longitude));
        }
    }
}

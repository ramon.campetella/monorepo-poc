using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MonorepoPOC.BackEnd.Infrastructure;
using MonorepoPOC.BackEnd.Interfaces.Infrastructure;
using Moq;
using Moq.Protected;
using Xunit;

namespace MonorepoPOC.BackEnd.Tests
{
    public class WeatherApiServiceTests
    {
       
        [Fact]
        public async Task GetWeatherAsync_ReturnsWeatherData_WhenSuccessful()
        {
            // Arrange
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(@"{
                    ""lat"": -34.6132,
                    ""lon"": -58.3772,
                    ""current"": {
                        ""dt"": 1717966227,
                        ""temp"": 21.22,
                        ""feels_like"": 21.43,
                        ""pressure"": 1004,
                        ""humidity"": 78
                    }
                }")
                });

            var httpClient = new HttpClient(mockHttpMessageHandler.Object)
            {
                BaseAddress = new Uri("https://api.openweathermap.org/")
            };

            var httpClientFactoryMock = new Mock<IHttpClientFactory>();
            httpClientFactoryMock
                .Setup(factory => factory.CreateClient(It.IsAny<string>()))
                .Returns(httpClient);

            var weatherApiService = new WeatherApiService(httpClientFactoryMock.Object);

            // Act
            var weather = await weatherApiService.GetWeatherAsync(0, 0);

            // Assert
            Assert.NotNull(weather);
            Assert.Equal(21.22, weather.Temperature);
            Assert.Equal(21.43, weather.FeelsLike);
            Assert.Equal(1004, weather.Pressure);
            Assert.Equal(78, weather.Humidity);
        }
    }
}

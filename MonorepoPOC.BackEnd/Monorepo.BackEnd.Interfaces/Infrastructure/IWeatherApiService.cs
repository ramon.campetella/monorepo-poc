﻿using Monorepo.BackEnd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonorepoPOC.BackEnd.Interfaces.Infrastructure
{
    public interface IWeatherApiService
    {
        Task<Weather> GetWeatherAsync(double latitude, double longitude);
    }
}

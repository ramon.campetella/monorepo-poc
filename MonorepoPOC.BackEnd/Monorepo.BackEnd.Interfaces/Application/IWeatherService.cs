﻿using Monorepo.BackEnd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonorepoPOC.BackEnd.Interfaces.Application
{
    public interface IWeatherService
    {
        Task<Weather> GetWeatherAsync(double latitude, double longitude);
    }
}

﻿namespace Monorepo.BackEnd.Domain
{
    public class Weather
    {
        public double Temperature { get; set; }
        public double FeelsLike { get; set; }
        public double Pressure { get; set; }
        public double Humidity { get; set; }
    }
}
import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import axios from 'axios';
import App from './App';
import '@testing-library/jest-dom/extend-expect';

// Mockear Axios
jest.mock('axios');

describe('App', () => {
  it('should fetch and display weather data when button is clicked', async () => {
    // Mockear la respuesta de la API
    const mockWeatherData = {
      temperature: 25,
      feelsLike: 26,
      pressure: 1010,
      humidity: 70,
    };
    axios.get.mockResolvedValue({ data: mockWeatherData });

    // Renderizar el componente
    const { getByText, getByLabelText } = render(<App />);

    // Ingresar valores de latitud y longitud en los campos de entrada
    const latitudeInput = getByLabelText('Latitude:');
    const longitudeInput = getByLabelText('Longitude:');
    fireEvent.change(latitudeInput, { target: { value: '40.7128' } });
    fireEvent.change(longitudeInput, { target: { value: '-74.0060' } });

    // Simular clic en el botón
    const button = getByText('Get Weather');
    fireEvent.click(button);

    // Esperar a que se muestre la información del clima
    await waitFor(() => {
      expect(getByText(`Temperature: ${mockWeatherData.temperature}`)).toBeInTheDocument();
      expect(getByText(`Feels Like: ${mockWeatherData.feelsLike}`)).toBeInTheDocument();
      expect(getByText(`Pressure: ${mockWeatherData.pressure}`)).toBeInTheDocument();
      expect(getByText(`Humidity: ${mockWeatherData.humidity}`)).toBeInTheDocument();
    });
  });
});

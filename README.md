# monorepo-poc
[![pipeline status](https://gitlab.com/ramon.campetella/monorepo-poc/badges/main/pipeline.svg)]

## Descripción
Prueba de concepto del uso de un monorepositorio
El sistema de prueba muestra el clima actual de una latitud/longitud cargada por el usuario.

Se utilizaron las siguientes tecnologias
Front end: REACT
Back End: .net 6

## Instalación
Front end:
Instalar el servidor node.js version 14.9.0
- CD FrontEnd/monorepo-poc
- npm install
- npm test
- npm start

Back end:
Instalar Visual Studio 2019 o superior
Instalar .net 6
- CD MonorepoPOC.BackEnd
- Abrir la solucion MonorepoPOC.BackEnd.sln


## Tipo de Licencia
Open source.

## Estado del proyecto
Alfa